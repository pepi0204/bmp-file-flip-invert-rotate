#ifndef BMP_HEADER_H
#define BMP_HEADER_H
#include <stdio.h>

typedef struct
{
    unsigned char value : 1;
    unsigned char reserved : 7;
}pixel1b;

typedef struct
{
    unsigned char value : 2;
    unsigned char reserved : 6;
}pixel2b;

typedef struct
{
    unsigned char value : 4;
    unsigned char reserved : 4;
}pixel4b;

typedef struct
{
    char value;
}pixel8b;

typedef union
{
    short value;
    struct{
        unsigned short blue_color : 5;
        unsigned short green_color : 6;
        unsigned short red_color : 5;
    }color;
}pixel16b;

typedef struct
{
    char blue_color;
    char green_color;
    char red_color;
}pixel24b;

typedef struct
{
    short   BMP;
    char    sizeOfFile[4];
    char    reserved[4];
    char    offsettoStartofImageData[4]; //138
    char    sizeofBMPInfoHeader[4]; // 40 124
    char    width[4];
    char    height[4];
    short   planesCnt; // 1
    short   bitsperPixel; // 24
    char    compressionType[4];             // 0
    char    sizeofImage[4];
}bmpHeader;

///////////////////////////////////////////////////////////////////////////
/// @brief set current width and height from in the header of input file
/// @param pHeader pointer to the header of input file
/// @param width width
/// @param height height
///////////////////////////////////////////////////////////////////////////
void input_dimentions(bmpHeader* pHeader, int width, int height);

//////////////////////////////////////////////////////////////////////////
/// @brief get current width and height from the header of input file
/// @param pHeader pointer to the header of input file
/// @param width width
/// @param height height
//////////////////////////////////////////////////////////////////////////
void get_current_dimentions(bmpHeader* pHeader, int* width, int* height);

//////////////////////////////////////////////////////////////////////////
/// @brief Calculate size of bytes between header and image data and 
///        allocate empty char array
/// @param mHeader Header of input file
/// @param p_offset pointer to int, number of bytes between header and 
///        image data
/// @return pointer to alocated char array
//////////////////////////////////////////////////////////////////////////
char* bmp_header_offset(bmpHeader mHeader,int* p_offset);

//////////////////////////////////////////////////////////////////////////
/// @brief Calculate size in bytes the Image data of the input file
/// @param mHeader Header of input file
/// @return size of image data
//////////////////////////////////////////////////////////////////////////
int get_sizeofImageData(bmpHeader mHeader);

#endif
