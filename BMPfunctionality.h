
#ifndef BMP_FUNCTIONALITY_H
#define BMP_FUNCTIONALITY_H

#include <stdio.h>
#include "BMPheader.h"

/////////////////////////////////////////////////////////////////////
/// @brief convert image data from 1 dimention char array to 2 dimention
///        pixel array
/// @param buffer pointer to image data of input file
/// @param mHeader header of input file
/// @return pointer to pixel array 
/////////////////////////////////////////////////////////////////////
void** BMP_Image_data_handler(char* buffer, bmpHeader mHeader);

/////////////////////////////////////////////////////////////////////
/// @brief flip image data horizontally
/// @param input pointer to non empty pixel array
/// @param pHeader pointer to header of input file
/// @return 0 if successfull, 1 if failed
/////////////////////////////////////////////////////////////////////
int horizontal_flip(void** input,bmpHeader* pHeader);

/////////////////////////////////////////////////////////////////////
/// @brief rotate image data 
/// @param input pointer to non empty pixel array
/// @param pHeader pointer to header of input file
/// @return pointer to pixel array
/////////////////////////////////////////////////////////////////////
void** rotate(void** input,bmpHeader* pHeader);

/////////////////////////////////////////////////////////////////////
/// @brief inverce every pixel in the image data
/// @param input pointer to non empty pixel array
/// @param pHeader pointer to header of input file
/// @return 0 if successfull, 1 if failed
/////////////////////////////////////////////////////////////////////
int Inverce(void** input,bmpHeader* pHeader);

#endif